package com.astra.dto.library.constant;

import lombok.Getter;

@Getter
public enum ETestCloud {

    FIRST_ENUM, SECOND_ENUM, THIRD_ENUM

}
