package com.astra.dto.library.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CommonDto {

    private Integer code;
    private String name;

}
